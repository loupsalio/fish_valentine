let a = 0
function angle(ax, ay, bx, by) {
	let a = { x: 0, y: 0 };
	let b = { x: 0, y: 0 };
	let c = { x: 0, y: 0 };

	a.x = ax;
	a.y = ay;

	b.x = bx;
	b.y = by

	c.x = ax;
	c.y = by;

	let ab = Math.sqrt(Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2));
	let ac = Math.sqrt(Math.pow(c.x - a.x, 2) + Math.pow(c.y - a.y, 2));

	let cosCAB = ac / ab;

	let CAB = Math.acos(cosCAB) * 180 / Math.PI;

	var angle = 180 - CAB;

	return { angle: angle, dist: 50 + ab * -0.10 };
}
var start = document.getElementsByTagName('h1')[0]


start.addEventListener("click", function () {
	document.getElementsByClassName('start')[0].style.display = "none";
	document.getElementsByClassName('wrapper')[0].style.visibility = "visible";

	let x = document.getElementsByClassName('fa-fish');

	document.addEventListener("mousemove", e => {
		Array.prototype.forEach.call(x, element => {
			arrow = element

			left = arrow.offsetLeft + 66
			xa = arrow.offsetTop + 60

			let t = angle(left, xa, e.pageX, e.pageY)
			let al = t.angle;

			arrow = element

			if (left > e.pageX && xa > e.pageY || (left < e.pageX && xa > e.pageY))
				al += 180;
			if ((left > e.pageX && xa < e.pageY) || (left < e.pageX && xa > e.pageY))
				al = al * -1;

			al -= 90

			arrow.style.webkitTransform = 'rotate(' + al + 'deg) rotateY(' + t.dist + 'deg)';
			arrow.style.mozTransform = 'rotate(' + al + 'deg) rotateY(' + t.dist + 'deg)';
			arrow.style.msTransform = 'rotate(' + al + 'deg) rotateY(' + t.dist + 'deg)';
			arrow.style.oTransform = 'rotate(' + al + 'deg) rotateY(' + t.dist + 'deg)';
			arrow.style.transform = 'rotate(' + al + 'deg) rotateY(' + t.dist + 'deg)';
		})

	})

})
